package main

import(
  "fmt"
  "net"
  "encoding/binary"
)


func reqServer(conn net.Conn){
  defer conn.Close()
  fmt.Println("Incoming connection from",conn.RemoteAddr())

  var headerbuf [16]byte
  var bufsize int =1500
  var buf []byte = make([]byte,bufsize)

  // Permanent loop
  for{
    // Get the header
    n, err := conn.Read(headerbuf[0:])
    sockCheckError(err)
    if n==0{
      return
    }

    // Extract length et request size from header
    var length,reqsz uint64
    reqsz = binary.LittleEndian.Uint64(headerbuf[0:8])
    length = binary.LittleEndian.Uint64(headerbuf[8:16])

    reqsz-=16

    // Read all data for current request
    for reqsz>0{
      n, err := conn.Read(buf[0:int(Uint64Min(reqsz,uint64(bufsize)))])
      sockCheckError(err)
      reqsz-=uint64(n)
    }


    // Reply if something is requested
    for length>0{
      // Send our reply
      n, err := conn.Write(buf[0:int(Uint64Min(uint64(len(buf)),length))])
      sockCheckError(err)
      length-= uint64(n)
    } 

  }
  return
}

