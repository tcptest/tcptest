package main

import(
  "sync"
  "net"
  "time"
)



type ReqClient struct{
  // Arrays of all requests in a connection
  Requests []Request `json:"requests"`
  // Time to start in ms
  Starttime uint32 `json:"starttime"`
  // Various utils : connection stats, connection object, mutex, waitgroup and
  // connection id
  s *Stats
  conn net.Conn
  mutex *sync.RWMutex
  wg *sync.WaitGroup
  id int
}

func (rc *ReqClient)Run(){
  // At the end of the function, we close the connection and display its stats
  defer rc.conn.Close()
  defer rc.s.DisplayStats()
  // We schedule a new goroutine for each request
  for _,req := range rc.Requests {
    rc.wg.Add(1)
    // Initialize request statistics, parent and id
    req.s = NewStats()
    req.parent = rc
    req.s.SetID(rc.id)
    go dataRequest(req)
  }
  // Don't quit until all requests have been processed
  rc.wg.Wait()
  return
}

func StartClient(rc ReqClient,tcpAddr *net.TCPAddr, wbuf,rbuf *int, wg *sync.WaitGroup){
  defer wg.Done()
  // If a timer was defined, wait for it
  if rc.Starttime != 0{
    timer := time.NewTimer(time.Millisecond*time.Duration(rc.Starttime))
    <- timer.C
  }
  // Establish connection to the server
  conn,err := net.DialTCP("tcp", nil,tcpAddr )
  checkError(err)
  // Set various options to socket
  setTCPOptions(conn)
  setBuffers(*conn,*wbuf,*rbuf)
  rc.conn=conn
  // Define the mutex and waitgroup that will be used to manage socket access
  // for all requests
  var wg2 sync.WaitGroup
  var mutex sync.RWMutex
  rc.wg=&wg2
  rc.mutex=&mutex
  rc.s=NewStats()
  rc.s.SetID(rc.id)
  rc.s.DisplayBurstTableHeader()
  // Launch request client
  rc.Run()
}
