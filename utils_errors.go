package main

import (
  "fmt"
  "io"
  "os"
  "syscall"
)

// Check for error and displays error message
func genericCheckError(message string, err error){
  if err != nil {
    fmt.Fprintf(os.Stderr, message+": %s\n", err.Error())
  }
}

// Check for error and quits if an error is happening
func checkError(err error) {
  if err != nil {
    fmt.Fprintf(os.Stderr, "Fatal error: %s\n", err.Error())
    os.Exit(1)
  }
}

// Check for socket error and displays message at the exception of EOF and EPIPE errors 
func sockCheckError(err error ){
      if err != nil {
        if err !=io.EOF && err!=syscall.EPIPE{
          fmt.Fprintf(os.Stderr, "Socket error: %v\n", err.Error())
        }
      }
}
