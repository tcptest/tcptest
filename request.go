package main

import(
  "time"
  "encoding/binary"
)
type Request struct{
  Starttime uint32 `json:"starttime"`
  Toreceive uint64 `json:"toreceive"`
  Tosend uint64 `json:"tosend"`
  s *Stats
  parent *ReqClient
}


func NewRequest(start uint32,toreceive,tosend uint64) *Request{
  r := Request{}
  r.Starttime = start
  r.Toreceive = toreceive
  r.Tosend = tosend
  r.s = NewStats()
  return &r
}

//var buf [1500]byte


func dataRequest(r Request){
  defer r.parent.wg.Done()
  defer r.parent.mutex.Unlock()

  // If a timer was defined, wait for it
  var timer *time.Timer
  if r.Starttime!=0{
    timer = time.NewTimer(time.Millisecond * time.Duration(r.Starttime))
    <- timer.C
  }
  // The connection
  r.s.ResetTimer()

  var bufsize int =1500
  var buf []byte = make([]byte,bufsize)

  // Put request size and length at begining of the packet
  binary.LittleEndian.PutUint64(buf[0:8], r.Tosend)
  binary.LittleEndian.PutUint64(buf[8:16], r.Toreceive)
  // Send our the full request
  r.parent.mutex.Lock()
  for r.Tosend>0{
    n, err := r.parent.conn.Write(buf[0:int(Uint64Min(uint64(bufsize),r.Tosend))])
    sockCheckError(err)
    r.Tosend-=uint64(n)
  }
  r.parent.mutex.Unlock()

  // Start burst statistics for both request and requestClient
  r.s.ResetTimer()
  r.parent.s.ResetTimer()

  // Wait for response
  r.dataSink()
  return
}


func (r *Request)dataSink(){
  defer r.s.DisplayBurstStats()
  defer r.parent.mutex.Unlock()

  var bufsize int =1500
  var buf []byte = make([]byte,bufsize)

  // Block on reception of the message
  r.parent.mutex.Lock()
  for r.Toreceive>0 {
    // Read all data from current request
    n, err := r.parent.conn.Read(buf[0:int(Uint64Min(r.Toreceive,uint64(bufsize)))])
    sockCheckError(err)

    // Compute statistics on received data
    r.Toreceive-=uint64(n)
    r.s.AddToStats(uint64(n))
    r.parent.s.AddToStats(uint64(n))
  }
  r.parent.mutex.Unlock()

  return
}

