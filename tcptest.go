package main

import (
  "fmt"
  "flag"
  "os"
  "net"
  "sync"
)



func main()  {
  // Arguments passed to the program
  var active = flag.Bool("active", false, "Basic Active socket")
  var client = flag.Bool("client",false, "Act as request/response client")
  var listen = flag.Bool("listen", false, "Basic Passive socket")
  var server = flag.Bool("server", false, "Act as a request/response server")
  //var server = flag.Bool("client",false, "Act as request/response client (need to precise file with -f)")

  var port = flag.Int("p",13000,"Source (passive socket) or destination (active socket) port")
  var rbuf = flag.Int("rb",0,"Set TCP read buffer")
  var wbuf = flag.Int("wb",0,"Set TCP write buffer")
  var tosendbyte = flag.Int("n",-1,"Number of bytes to send")
  var tosendpkt=flag.Int("N",-1,"Number of packets to send (need to precise size with -s)")
  var pktsz=flag.Int("s",-1,"Packet size")
  var dur=flag.Int("d",-1,"Time (in seconds) to emit continuously")
  var file=flag.String("f","","Configuration File")
  flag.Parse()
  var remaining_args=flag.Args()

  // Check for IP address
  var ip string
  if len(remaining_args) ==1 {
    ip=remaining_args[0]
  } else if *listen || *server{
    ip="0.0.0.0"
  } else{
    fmt.Fprintf(os.Stderr,"You need to specify the destination IP\n")
    return
  }

  // Check some incompatible options
  if (*active && *listen) || (*listen && *client) || (*server && *client) || (*server && *active)  {
    fmt.Fprintf(os.Stderr,"TCPtest can't be listening and connecting at the same time\n")
    return
  } else if *active && *client {
    fmt.Fprintf(os.Stderr,"TCPtest can't be active and sending requests at the same time\n")
    return
  } else if *listen && *server{
    fmt.Fprintf(os.Stderr,"TCPtest can't be passive and replying to requests at the same time\n")
    return
  } else if !*active && !*listen && !*client && !*server {
    fmt.Fprintf(os.Stderr,"TCPtest is currently doing nothing\n")
    return
  } 

  if *client && *file==""{
    fmt.Fprintf(os.Stderr,"Please specify configuration file")
    return
  }

  if *tosendpkt>0 && *pktsz<0{
    fmt.Fprintf(os.Stderr,"You must specify the packet size when sending a number of packets (-s)\n")
    return
  }

  // Create "ip:port" server address
  servAddr := ip+":"+fmt.Sprintf("%v",*port)
  tcpAddr, err := net.ResolveTCPAddr("tcp", servAddr)
  checkError(err)

  // This function is used for initializing maps for displaying numbers
  initMaps()

  if *listen || *server{
    // Listening socket
    ln, err := net.ListenTCP("tcp",tcpAddr)
    checkError(err)
    for {
      conn, err := ln.AcceptTCP()
      fmt.Println("New Connection")
      checkError(err)
      // Set various options to socket
      setTCPOptions(conn)
      setBuffers(*conn,*wbuf,*rbuf)
      // Different functions for different options
      if *server{
        go reqServer(conn)
      }else{
        go listener(conn)
      }
    }
  } else if *active{
    // Active socket, connecting to the server
    conn,err := net.DialTCP("tcp", nil,tcpAddr )
    checkError(err)
    // Set various options to socket
    setTCPOptions(conn)
    setBuffers(*conn,*wbuf,*rbuf)
    sender(conn,*tosendbyte,*tosendpkt,*pktsz,*dur)
  }else{
    // Parse configuration file containing requests
    rqs:=parseJson(*file)
    // For each connection, start a new goroutine and wait for all to complete
    // before exiting
    var wg sync.WaitGroup
    for index,rc := range rqs.Reqclients{
      wg.Add(1)
      rc.id=index
      go StartClient(rc,tcpAddr,wbuf,rbuf,&wg)
    }
    wg.Wait()
  }
}
