package main

import (
  "syscall"
  "net"
)




func setBuffers(conn net.TCPConn,wbuf,rbuf int){
  // Set read and write buffers
  if wbuf!=0{
    conn.SetWriteBuffer(wbuf)
  }
  if rbuf!=0{
    conn.SetReadBuffer(rbuf)
  }
}


func setTCPOptions(conn *net.TCPConn){
  // Remove Nagle algorithm
  conn.SetNoDelay(true)
  //file, err := conn.File()
  //checkError(err)
  //defer file.Close()
  //fd := file.Fd()
  // Activate TCP CORK
  //err=syscall.SetsockoptInt(int(fd), syscall.SOL_TCP, syscall.TCP_CORK,1)
  //genericCheckError("Error activating TCP_CORK", err)
  //// Disable Nagle's algorithm
  //err=syscall.SetsockoptInt(int(fd), syscall.SOL_TCP, syscall.TCP_NODELAY,1)
  //genericCheckError("Error activating TCP_NODELAY", err)
}

func setListenerOptions(ln *net.TCPListener){
  // Get File descriptor of TCP Socket
  file, err := ln.File()
  checkError(err)
  defer file.Close()
  fd := file.Fd()
  // Activate SO_REUSEADDR
  err=syscall.SetsockoptInt(int(fd), syscall.SOL_SOCKET, syscall.SO_REUSEADDR,1)
  genericCheckError("Error activating TCP_REUSEADDR", err)
}

