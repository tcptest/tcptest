package main

import (
  "fmt"
  "time"
)


type Stats struct{
  // Total number of bytes, number of bytes since last periodic display
  bytes,tmpbytes uint64
  // Total duration of connection, time elapsed since last periodic display
  time,tmptime time.Duration
  // Start time, TODO: checkit
  starttime,tmpstarttime,globalstarttime time.Time
  // Connexion id
  id int
}

func NewStats() *Stats{
  s := Stats{}
  s.bytes = 0
  s.time = 0
  s.starttime = time.Now()
  s.tmpbytes = 0
  s.tmptime = 0
  s.tmpstarttime = time.Now()
  s.globalstarttime = time.Now()
  s.id=0
  return &s
}

func (s *Stats)SetID(id int){
  s.id=id
}

func (s *Stats)AddToStats(bytes uint64){
  s.bytes+=bytes
  s.tmpbytes+=bytes
  now := time.Now()
  now2 := now.Sub(s.starttime)
  now3 := now.Sub(s.tmpstarttime)
  s.tmpstarttime=now
  s.starttime = now
  s.time+=now2
  s.tmptime+=now3
}


func (s *Stats)DisplayTableHeader(){
  fmt.Println("Time\t|\tKBytes\t|\tThroughput (kb/s)")
}

func (s *Stats)DisplayBurstTableHeader(){
  fmt.Printf("Cx n° %d\t|\tDuration\t|\tKbytes\t|\tStart\n",s.id)
}

// TODO: Faire ça propre aussi nondedieu
func (s *Stats)DisplayPeriodicStats(scheduled *time.Ticker){
    for _ = range scheduled.C {
      if int(s.tmptime.Seconds()*1000) !=0{
        fmt.Printf("%.2fs\t|\t%d\t|\t%d\n",time.Now().Sub(s.globalstarttime).Seconds(),s.tmpbytes/1000,int(s.tmpbytes*8)/int(s.tmptime.Seconds()*1000))
      } else if int(s.tmptime.Nanoseconds()*1000) !=0{
        fmt.Printf("%.2fs\t|\t%d\t|\t%d\n",time.Now().Sub(s.globalstarttime).Seconds(),s.tmpbytes/1000,int(s.tmpbytes*8)/int(s.tmptime.Nanoseconds()*1000))
      }
      s.tmpbytes=0
      s.tmptime=0
    }
}



// TODO : faire ça propre !
func (s *Stats)DisplayStats(){
  fmt.Printf("Connection %d - %sB in %s - Throughput %sb/s - Total time %d ns\n",s.id,formatBytes(int64(s.bytes),2),formatTime(s.time.Nanoseconds(),3),formatThroughput(8*float64(s.bytes)/float64(s.time.Nanoseconds()),3),time.Now().Sub(s.globalstarttime).Nanoseconds())
}

//Garbage
func (s *Stats)ResetTimer(){
  s.starttime = time.Now()
  s.tmpstarttime = time.Now()
  s.globalstarttime = time.Now()
}

func (s *Stats)DisplayBurstStats(){
    fmt.Printf("%d\t|\t%.6fs\t|\t%d\t|\t%v\n",s.id,time.Now().Sub(s.globalstarttime).Seconds(),s.tmpbytes/1000,s.globalstarttime)
    s.tmpbytes=0
    s.tmptime=0
}
