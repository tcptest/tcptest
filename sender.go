package main

import (
  "fmt"
  "time"
  "net"
  "os"
  "os/signal"
  "syscall"
)


// Proxy function
func sender(conn net.Conn, tosendbyte, tosendpkt,pktsz,dur int ){
  send_data(conn,tosendbyte,tosendpkt,pktsz,dur)
}


func send_data(conn net.Conn,tosendbyte,tosendpkt,pktsz,dur int){

  // Take into account various options passed to the program
  if tosendpkt>0{
    tosendbyte = tosendpkt*pktsz
  }
  if pktsz<0{
    pktsz=150000
  }

  // Initialize stats
  var s *Stats = NewStats()
  s.DisplayTableHeader()
  defer s.DisplayStats()
  
  // Schedule periodic display of stats on screen
  t:=time.NewTicker(time.Second * 1)
  defer t.Stop()
  go s.DisplayPeriodicStats(t)

  // Whatever the result of the function, we close the function
  defer conn.Close()


  // We send data packet by packet
  var buf = make([]byte,pktsz)

  // Initialize signal handler to catch SIGINT and SIGTERM
  sigs := make(chan os.Signal, 1)
  signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

  for {
    // Signal handler
    select {
      case  <-sigs: 
        fmt.Println("")
        return
      default:
    }

    if tosendbyte<0 && tosendpkt <0{
      // Continuously send data on the network
      n, err := conn.Write(buf[0:])
      // If we weren't able to write, it's time to quit
      if n==0{
        return
      }
      sockCheckError(err)
      s.AddToStats(uint64(n))
      // If a duration was specified, check if it hasn't expired
      if dur>0 && s.time.Seconds()>float64(dur) {
        return
      }
    } else if tosendbyte>0{
      // Send one packet of data and quit if everything was sent
      n, err := conn.Write(buf[0:IntMin(tosendbyte,pktsz)])
      // If we weren't able to write, it's time to quit
      if n==0{
        return
      }
      sockCheckError(err)
      tosendbyte-=n
      s.AddToStats(uint64(n))
      if tosendbyte<=0{
        return
      }
    }
  }
  return
}

