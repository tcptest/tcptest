package main

import (
  "encoding/json"
  "io/ioutil"
)

// Here just to facilitate json parsing
type Connections struct{
  Reqclients []ReqClient
}

func parseJson(fileName string) Connections{
  // Open the file
  jsonBlob,err := ioutil.ReadFile(fileName)
  checkError(err)

  // Basically let the json magic work to fill the structs
  var reqclients Connections
  err= json.Unmarshal(jsonBlob, &reqclients)
  checkError(err)

  return reqclients
}
