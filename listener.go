package main

import (
  "fmt"
  "net"
  "time"
)

func listener(conn net.Conn){
  fmt.Println("Incoming connection from",conn.RemoteAddr())

  // Configure stats
  var s *Stats = NewStats()
  s.DisplayTableHeader()
  defer s.DisplayStats()

  // Configure periodic stats display
  t :=time.NewTicker(time.Second * 1)
  defer t.Stop()
  go s.DisplayPeriodicStats(t)

  defer conn.Close()

  // Read big chunks of data up to the end of the connection
  var buf [15000]byte
  for {
    n, err := conn.Read(buf[0:])
    if n==0{
      return
    }
    sockCheckError(err)
    s.AddToStats(uint64(n))
  }
  return
}
