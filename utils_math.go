package main

// Redefinition of min and max functions as the math package only provides min
// and max functions for float64

func IntMin(a,b int) int{
  if a<b{
    return a
  } else{
    return b
  }
}

func IntMax(a,b int) int{
  if a>b{
    return a
  } else{
    return b
  }
}

func Uint64Min(a,b uint64) uint64{
  if a<b{
    return a
  } else{
    return b
  }
}
