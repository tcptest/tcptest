package main

import (
  "strconv"
)

var debug = false
var bytemap map[int]string
var secondmap map[int]string


// Initialize the maps once and for all
func initMaps(){
  bytemap = make(map[int]string)
  bytemap[0]=""
  bytemap[1]="K"
  bytemap[2]="M"
  bytemap[3]="G"
  secondmap = make(map[int]string)
  secondmap[0]="ns"
  secondmap[1]="us"
  secondmap[2]="ms"
  secondmap[3]="s"
}


// Get the order of a number (up to a maximum value)
func getOrder(number int64,maxorder int) (string,string,int){
  var divider int64
  divider=1000
  for i:=0;i<maxorder;i++{
    divider*=1000
  }
  // Decrease divider up to the point where the result of the division of the
  // number by the divider is equal to 0
  for i:=maxorder;i>0;i--{
      divider/=1000
    if int64(number/divider)>0{
      return strconv.FormatInt(int64(number/divider),10),strconv.FormatInt(number%divider,10),i
    }

  }
  return strconv.FormatInt(number,10),"0",0
}


// The three following functions find the order of the number passed as an
// argument and return a string composed of the number accompaigned by the
// corresponding unit

func formatBytes(number int64,dec int) string{
  numstr,decimals,order:=getOrder(number,3)
  if dec==0 || order==0{
    return numstr+bytemap[order]
  } else{
    dec=IntMin(len(decimals),dec)
    return numstr+"."+decimals[:dec]+bytemap[order]
  }
}

func formatTime(number int64,dec int) string{
  numstr,decimals,order:=getOrder(number,3)
  if dec==0 || order==0{
    return numstr+secondmap[order]
  } else{
    dec=IntMin(len(decimals),dec)
    return numstr+"."+decimals[:dec]+secondmap[order]
  }
}

func formatThroughput(number float64, dec int) string{
  number*=1000000000
  numint:=int64(number)
  numstr,decimals,order:=getOrder(numint,3)
  if dec==0 || order==0{
    return numstr+bytemap[order]
  } else{
    dec=IntMin(len(decimals),dec)
    return numstr+"."+decimals[:dec]+bytemap[order]
  }
}


